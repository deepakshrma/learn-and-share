import { Gulp } from 'gulp';
export interface IPlugin {
    run: (...args: any) => void;
}
export declare class PluginBase {
    gulp: Gulp;
    colors: any;
    constructor();
    toString(): string;
}
