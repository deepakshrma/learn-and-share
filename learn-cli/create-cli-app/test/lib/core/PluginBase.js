"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var safe_1 = __importDefault(require("colors/safe"));
var gulp_1 = __importDefault(require("gulp"));
safe_1.default.setTheme({
    input: 'grey',
    silly: 'rainbow',
    verbose: 'cyan',
    prompt: 'grey',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'yellow',
    debug: 'blue',
    error: 'red',
});
var PluginBase = /** @class */ (function () {
    function PluginBase() {
        this.gulp = gulp_1.default;
        this.colors = safe_1.default;
    }
    PluginBase.prototype.toString = function () {
        var funcNameRegex = /function (.{1,})\(/;
        var results = funcNameRegex.exec(this.constructor.toString());
        return results && results.length > 1 ? results[1] : '';
    };
    return PluginBase;
}());
exports.PluginBase = PluginBase;
