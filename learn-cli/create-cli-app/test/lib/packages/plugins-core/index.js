"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var PluginBase_1 = require("../../core/PluginBase");
var CorePlugin = /** @class */ (function (_super) {
    __extends(CorePlugin, _super);
    function CorePlugin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CorePlugin.prototype.run = function () {
        var _a = this.gulp, src = _a.src, dest = _a.dest;
        var info = this.colors.info;
        function copy() {
            return src('src/**/*').pipe(dest('output/'));
        }
        console.info(info('test'));
        copy();
    };
    return CorePlugin;
}(PluginBase_1.PluginBase));
exports.build = function (yargs) {
    yargs.command('serve [port]', 'start the server', function (argv) {
        argv.positional('port', {
            describe: 'port to bind on',
            default: 5000,
        });
        return argv;
    }, function (argv) {
        var plugin = new CorePlugin();
        if (argv.verbose) {
            console.info("runing plugin : " + plugin);
        }
        plugin.run();
    });
};
