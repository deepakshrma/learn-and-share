"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var PluginBase_1 = require("../../core/PluginBase");
var ClonePlugin = /** @class */ (function (_super) {
    __extends(ClonePlugin, _super);
    function ClonePlugin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClonePlugin.prototype.run = function (name) {
        console.log('runing...' + name);
    };
    return ClonePlugin;
}(PluginBase_1.PluginBase));
exports.build = function (yargs) {
    yargs.command('clone', 'Clone is new plugin', function (argv) {
        argv.positional('name', {
            describe: 'default plugin name',
            default: 'default',
        });
        return argv;
    }, function (argv) {
        var plugin = new ClonePlugin();
        if (argv.verbose)
            console.info("[verbose]runing plugin : " + plugin);
        plugin.run(argv.name);
    });
};
