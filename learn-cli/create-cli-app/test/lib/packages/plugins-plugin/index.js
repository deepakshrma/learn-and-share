"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var gulp_replace_1 = __importDefault(require("gulp-replace"));
var PluginBase_1 = require("../../core/PluginBase");
var CreatePlugin = /** @class */ (function (_super) {
    __extends(CreatePlugin, _super);
    function CreatePlugin() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CreatePlugin.prototype.run = function (name) {
        var _a = this.gulp, src = _a.src, dest = _a.dest;
        name = name.toLowerCase();
        var capsName = name.charAt(0).toUpperCase() + name.substr(1);
        src('templates/new_plugin/*')
            .pipe(gulp_replace_1.default(/\<Name>/g, capsName))
            .pipe(gulp_replace_1.default(/\<name>/g, name))
            .pipe(dest("src/packages/plugins-" + name));
    };
    return CreatePlugin;
}(PluginBase_1.PluginBase));
exports.build = function (yargs) {
    yargs.command('np [name]', 'create new plugin', function (argv) {
        argv.positional('name', {
            describe: 'default plugin name',
            default: 'new-plugin',
        });
        return argv;
    }, function (argv) {
        var plugin = new CreatePlugin();
        if (argv.verbose) {
            console.info("runing plugin : " + plugin);
        }
        plugin.run(argv.name);
    });
};
