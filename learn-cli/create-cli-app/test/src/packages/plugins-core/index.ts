import figlet from 'figlet';
import { Argv } from 'yargs';
import { IPlugin, PluginBase } from '../../core/PluginBase';


class CorePlugin extends PluginBase implements IPlugin {
	public run() {
		// const { src, dest } = this.gulp;
		figlet('Hello World!!', (err, data) => {
			if (err) {
				console.info('Something went wrong...');
			}
			console.info(data)
		});
	}
}
export const build = (yargs: Argv) => {
	yargs.command(
		'new [project_name]',
		'Create a new project',
		(argv: Argv) => {
			argv.positional('project_name', {
				describe: 'name of project_name',
				default: "webstudio-app",
			});
			return argv;
		},
		(argv: any) => {
			const plugin = new CorePlugin();
			if (argv.verbose) {
				console.info(`runing plugin : ${plugin}`);
			}
			plugin.run();
		},
	);
};
