import figlet from 'figlet';
import inquirer from 'inquirer';
import { Argv } from 'yargs';

import logger from '../../core/Logger';
import { IPlugin, PluginBase } from '../../core/PluginBase';
import { clearString, extract, resolvePath, stringify } from '../../core/Utils';

figlet.defaults({
	horizontalLayout: 'default',
	verticalLayout: 'default'
});

const printText = (text: string): string => {
	return figlet.textSync(text, { font: 'Small' });
};
class CorePlugin extends PluginBase implements IPlugin {
	public run(projectName) {
		const { help } = this.colors;
		console.info(help(printText("DBS Webstudio")));
		inquirer.prompt([
			{
				type: 'input',
				name: 'dlsVersion',
				message: 'Please enter dls version',
				default: 'dls-v3.0',
			},
			{
				type: 'list',
				name: 'templateName',
				message: 'Please choose your template',
				default: 0,
				choices: [{
					name: 'Basic',
					value: 'basic'
				},
				{
					name: 'Basic + SideNav',
					value: 'basic|sidenav'
				},
				{
					name: 'Basic + SideNav + PreLogin',
					value: 'basic|sidenav|prelogin'
				}]
			},
			{
				type: 'checkbox',
				name: 'mfes',
				message: 'Please choose required micro-modules(mfe)',
				choices: [{
					name: 'Fund Transfer',
					value: 'fundtranfer'
				},
				{
					name: 'Chatbot',
					value: 'chatbot'
				}]
			}
		]).then((answers) => {
			logger.warn(`You have selected:\n${stringify(answers)}`);
			this.createProject(projectName, logger.error)
		}).catch((message) => logger.error(message));
	}
	private createProject(projectName, done?: (error) => void) {
		const { cwd, home } = this.options;
		console.info(home);
		console.info(resolvePath(home, 'repos', 'dls-v3.0.zip'));
		logger.warn("Started extraction");
		extract(resolvePath(home, 'repos', 'dls-v3.0.zip'), resolvePath(cwd, clearString(projectName)), (error) => {
			if (error) {
				logger.error(error)
			}
		})
	}
}
export const build = (yargs: Argv) => {
	yargs.command(
		'new [project_name]',
		'Create a new project',
		(argv: Argv) => {
			argv.positional('project_name', {
				describe: 'name of project_name',
				default: "webstudio-app",
			});
			return argv;
		},
		(argv: any) => {
			const plugin = new CorePlugin();
			logger.setVerbose(argv.verbose)
			logger.log(`Runing plugin : ${plugin}`);
			plugin.run(argv.project_name);
		},
	);
};
