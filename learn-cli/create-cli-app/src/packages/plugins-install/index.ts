import { Argv } from 'yargs';
import logger from '../../core/Logger';
import { IPlugin, PluginBase,  } from '../../core/PluginBase';
import { clearString, extract, resolvePath,  } from '../../core/Utils';

class InstallPlugin extends PluginBase implements IPlugin {
  public run(version: string) {
    const { cwd, home } = this.options;
		console.info(home);
		console.info(resolvePath(home, 'repos', 'dls-v3.0.zip'));
		logger.warn(`Installing modules version: ${version} ...`);
		extract(resolvePath(home, 'modules', 'modules-dls-v3.0.zip'), cwd, (error) => {
			if (error) {
				logger.error(error)
			}
		})
  }
}
export const build = (yargs: Argv) => {
  yargs.command(
    'install [version]',
    'Install node_module based on version',
    (argv: Argv) => {
      argv.positional('version', {
        describe: 'default plugin name',
        default: 'dls-v3.0',
      });
      return argv;
    },
    (argv: any) => {
      const plugin = new InstallPlugin();
      if (argv.verbose) {
        console.info(`[verbose]Runing plugin : ${plugin}`)
      };
      plugin.run(argv.version);
    },
  );
};
