import colors_ from 'colors/safe';
colors_.setTheme({
    input: 'grey',
    silly: 'rainbow',
    verbose: 'cyan',
    prompt: 'grey',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'yellow',
    debug: 'blue',
    error: 'red',
});
const colors: any = colors_;
let verbose: boolean = false;
const rawLog = (message, ...args) => {
    if(verbose) {
        console.info(colors.input(message), ...args);
    }
}
export const log = (message, ...args) => rawLog(colors.verbose(message), ...args)
export const info = (message, ...args) => rawLog(colors.info(message), ...args)
export const warn = (message, ...args) => console.info(colors.warn(message), ...args)
export const error = (message, ...args) => console.info(colors.error(message), ...args)

export const setVerbose = (isVebose) => verbose = isVebose;
export default {
    log,
    info,
    warn,
    error,
    setVerbose
}