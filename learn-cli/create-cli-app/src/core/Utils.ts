import decompress from 'decompress'
import tgz from 'decompress-targz'
import unzip from 'decompress-unzip'
import path from 'path'

export const extract = (src, dest, cb = (error?: any) => error) => {
    const ext = path.extname(src) || 'zip';
    decompress(src, dest, {
        plugins: [
            (ext === '.zip' ? unzip() : tgz())
        ]
    }).then((files) => {
        if (files.length) {
            cb();
        } else {
            cb(new Error(('Error while extracting compressed file')));
        }
    }).catch((error) => {
        cb(error);
    });
}
export const sleep = (ms: number) => {
    const now = new Date().getTime();
    while (new Date().getTime() < now + ms) { /* do nothing */ }
}
export const clearString = (str: string) => {
    return str.replace(/\W+/g, "-").replace(/\w+/g, (word) => word.toLowerCase());
}
export const resolvePath = (filepath, ...args) => {
    if (filepath[0] === '~') {
        filepath = path.join(process.env.HOME || '', filepath.slice(1))
    }
    return path.resolve(filepath, ...args);
}
export const stringify = (object: any) => JSON.stringify(object, null, 4)
